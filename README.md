# View Site

[primedesign.gitlab.io](https://primedesign.gitlab.io/)

# How To Use

## Setting up

Create a git account

Get the source

## Installing
**Snippet Hugo (https://gohugo.io/getting-started/installing)**

Download the appropriate version for your platform from Hugo Releases. Once downloaded, the binary can be run from anywhere. You don’t need to install it into a global location. This works well for shared hosts and other systems where you don’t have a privileged account.

Ideally, you should install it somewhere in your PATH for easy use. /usr/local/bin is the most probable location.

[Download Hugo](https://github.com/gohugoio/hugo/releases/latest)

## Running

 hugo server

## When ready nto give to everybody else

commit changes, which invoves staging files and then pushing

when programs make that process easier with an interface

[ATOM] (https://atom.io/)

[Visual studio code] (https://code.visualstudio.com/)

[Gitkraken] (https://www.gitkraken.com/)

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.
