---

title: Week 5

subtitle: To the next part of the process

date: 2017-08-23 00:00:00 +0000

publishdate: 2017-08-23 00:00:00 +0000

tags: [ "Alex" ]

---

## Spark

*   Talk to students, staff and institute
*   Gained some knowledge from students of what the want and wish from the space

## Day 1 (23 August)

*   Mind maps based on the new knowledge
*   Talk with Lindra about the directions of wintec
*   Trigger questions, the pains and gains of learning spaces for students and teachers\
*   Removed questions we made at the start of the design factory

## Day 2 (25 August)

*   Personas from new knowledge gathered

## Upcoming for next week

*   Meeting with the customers (IT and resources) will hopefully discover more on the project