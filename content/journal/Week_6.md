---

title: Week 6

subtitle: Prototypes

date: 2017-08-28 00:00:00 +0000

publishdate: 2017-08-28 00:00:00 +0000

tags: [ "Alex" ]

---

# Day 1 (30 August)

*   Group building
*   Timeline
*   Bring up information from new interviews, added more insights
*   Two groups made 8 by 8 ideas of what a space should have, this will be used to consider what a challenge learning space will have.

## Day 2 (1 September)

*   Themes and insights
*   Ideated
*   Refined ideas
*   Prototyping
*   "Put ideas into physical ideas"

## Upcoming for next week

*   Teaching/Feedback for users
*   Begin Documentation for the site