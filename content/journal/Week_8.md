---

title: Week 8

subtitle: Present

date: 2017-09-14 00:00:00 +0000

publishdate: 2017-09-14 00:00:00 +0000

tags: [ "Alex" ]

bigimg: [{ src: "/images/Workshop_With.jpg", desc: "Activity with Executives" }, { src: "/images/Workshop_With2.jpg", desc: "Activity with Executives" } ]

---

# Day 1 (13/09)

*   Present to Executives
*   Seemed to have liked it
*   Suggested to present with seniors
*   Did an activity with Executives of making a space with the ideas we presented

## Current ideas

### Environment

"U - Work"(Individual work areas): • Individual work areas. • quiet spaces. • variety of group work spaces.

"Move'em Out"(Flexible Furniture): • Configurable to the persons needs. • Can move for more available room • Collapsible • Variety of furniture ◦ Tall tables ◦ Small tables

### Technology

"Quick Roll"(Swipe card roll) • Use id cards to register students in the class • Start class sooner Teacher instantly knows who is there

Image 2 "Smart Table"(Touch screen+ Whiteboard): • Touchscreen table. • white board. • raisable to present to class. • share ideas with class mates.

“Two view” (mirror display): • tutor can send their screen to the students so students can follow along. • students can connect to a cow or big screen to show work or video on their phone.

### Teaching and learning approaches

"Rail Desk": • Rail which boards and screens can move around on. • Addable Attachments.

U Page(Moodle): • personalized LMS/Moodle. • Add your own links. • Adaptable Learning.

Images 3 "Pre-class"(Pre-settable class): • Preset class. • tutor swipes their card and the projector and tv posters load up with todays lessons and resources.

Image 4: Wintec TV(Recording class): • Mobile Learning • Allow continues Learning • Allow students to catch up

Image 4 "TV Posters/Digital Poster • Personalization • Wintec updates • visual aids

## Day 2 (15/09)

*   Take a break/ Breather

## Upcoming for next week

*   Prototype and test the ideas