---

title: "Week 10"

subtitle: "Prepare for breakfast, refine Prototypes"

date: "2017-10-06"

publishdate: "2017-10-06"

tableofcontents: false

tags: [ "Alex" ]

---

## Day 1 (03/10)

*   Didn't happen people were busy. May need to make a new calender of times we are available

## Day 2 (04/10)

### Photo album

Powerpoint labeling what design factory/Prime design been doing

### Organize Industry breakfast

Prepare for the breakfast on wednesday

### Personas

Work on personas

### Identify who we going to user test on

## Day 3 (06/10)

Talk with Brad and Graeme Ward

Analyse feedback

Personas

2 done

## Next Week

*   Prepare for wednesday breakfast
*   Check for any new research
*   Check on personas
*   Lotus two new ideas