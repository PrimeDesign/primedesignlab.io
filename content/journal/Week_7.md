---

title: Week 7

subtitle: Refine & Prepare

date: 2017-09-04 00:00:00 +0000

publishdate: 2017-09-04 00:00:00 +0000

tags: [ "Alex" ]

---

# Day 1 (06/09)

*   Talked with Heith Sawyer
*   Teaching and learning coach
*   Did a design print
*   Gave insights
*   Incorporated Heiths insights into our insights
*   Timetable of what we will do this week to prepare for presentation
*   Team building activities
*   Drinking game
*   Heuristic ideation tools
*   Did storyboards, to plan for what our presentation will be about

## Day 2 (08/09)

*   Finalize documents
*   Prepare powerpoint to present to customer
*   Select best idealizations
*   Refine themes

## Upcoming for next week

*   Show editor
*   Present