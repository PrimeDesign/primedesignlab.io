---
title: Week 4

subtitle: The Beginning

date: 2017-08-18 00:00:00 +0000

publishdate: 2017-08-15 00:00:00 +0000

tags: [
"Alex",
"Spark"
]

bigimg: [
  {
  src: "/images/Interview_Week4.jpg",
  desc: "Interview"
  },
  {
  src: "/images/Interview2_Week4.jpg",
  desc: "Interview"
  },
  {
  src: "/images/Interview3_Week4.jpg",
  desc: "Interview"
  }
]

---
## Breakfast (16 August)

### Makebado

Queenstown airport

* moving away from the classic way system

Research found:

* People use words of images

Strategy:

* Large images to put across the message
* Budget was small so had to be more practical than creative
* To be different
* Use companies color palette
* Use Maori patterns
  * (unsure why Maori patterns)
  * Used to create a path

### Kris Sowersby

Typeface

* Typefaces are difficult to develop because of prerequisites

* Type faces have trends (they come and go and back again)
* Typefaces like to have a history have references to what come up with the idea
* Kris Wants his types to be a lasting font (a font to live through the ages)
* to be different

### Sarah

Graphics designer

Sarah believes much of graphics design is a call and response kind of process

Graphics design can be wrong

### (Name)

Art Design

* Not aimed to be something different
* Each venue had an identity
* As did the fonts
* Venues were connected together
* For the books made what was best to have done was to have had templates rather than a fixed design

## Day 1 (16 August)

What was the points from the students you interviewed

Empathy map

* What was the person feeling, thinking

Interview with staff

Followed by a Empathy map

## Day 2 (18 August)

Personas

* Create a persona (student and teacher) from the data we gathered from day 1

* We have a name, we are now Prime Design.

* Enter the ‘Define’ phase

## Powerpoint

[Week 4a Notes](/pdf/Week_4a_Notes.pdf)

[Week 4b Notes](/pdf/Week_4b_Notes.pdf)

## Upcoming for next week

* Talk with team members to get feedback on IT mini projects
* Breakfast with industry partners