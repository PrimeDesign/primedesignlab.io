---

title: Week 3

subtitle: Interviews are cool

date: 2017-08-11 00:00:00 +0000

publishdate: 2017-08-10 00:00:00 +0000

tags: [
"Alex"
]

bigimg:
- src: "/images/Group_Design.jpg"
  desc: Group
- src: "/images/Present_Design.jpg"
  desc: Present
- src: "/images/Day_2.jpg"
  desc: Day 2
- src: "/images/What_We_Know.jpg"
  desc: What We Know
- src: "/images/What_We_Dont_Know.jpg"
  desc: What We Don't Know
- src: "/images/Drawings1.jpg"
  desc: Drawing
- src: "/images/Drawings2.jpg"
  desc: Drawing

---
# Day 1 (9 August)

* Team building
  * Super hero

  Get everybody energized and to begin considering other people

* Interviews
  * Business Partners
    * What they want
    * What they know about wintec

Use a more formal way of understanding what  Business Partners want

* Students
  * They experience in spaces (Lecture experience, assignments experience, personal experience)

For the design hub to get some current information on what staff and students think of learning spaces, what doesn't work etc.

## Day 2 (11 August)

* See how the research turned out
* Present Questions
* Refine Questions

Get everyone on the same level of what we know

## Powerpoint

[Week 3 Notes](/pdf/Week_3_Notes.pdf)

## Upcoming for next week

* See the answers from the questions that we used to interview students