---

title: "Week 11"

subtitle: "Another Breakfast"

date: "2017-10-13"

publishdate: "2017-10-13"

tableofcontents: false

tags: [ "Alex" ]

bigimg: [{ src: "/images/Breakfast_Week 11_1.jpg", desc: "Breakfast" }, { src: "/images/Breakfast_Week 11_2.jpg", desc: "Breakfast" }, { src: "/images/Breakfast_Week 11_3.jpg", desc: "Breakfast" }, { src: "/images/Breakfast_Week 11_4.jpg", desc: "Breakfast" }, { src: "/images/Breakfast_Week 11_5.jpg", desc: "Breakfast" }, { src: "/images/Breakfast_Week 11_6.jpg", desc: "Breakfast" } ]

---

## Day 1 (10/10)

*   Prepare for Breakfast
*   Refine Prototypes

## Day 2 (11/10)

*   Breakfast
*   Refine Prototypes
*   Start making a floor plan
*   Refine pre-class
*   Identify who and where we test prototypes

## Day 3

*   Timeline
*   Add more detail on what we will do
*   Plan for user testing
*   Assign who will do what research