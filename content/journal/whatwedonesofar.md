---

title: What we have done so far

subtitle: Summary

date: 2017-10-19 00:00:00 +0000

tags: [ "Recap" ]

tableofcontents: true

---

# Week 1

*   We discovered our problem
*   Who the team members are
*   Team building activities
*   2 Truths and 1 lie
*   Create an image based on yourself
*   Did a personality test to discover what quality you most fit

# Week 2

*   Create Questions on multiple boards to understand the Issue
*   The goal of this is to break down the issue we have been given into possible other issues to try understand what this issue entails
*   Introductions to both project members and Clients
*   Clients contributed questions and answered some of the questions.
*   Preparations for interview
*   Interview Theory
*   Prepare questions
*   Interviewed Staff
*   Conclusions
*   Project members did a group activity about what they liked and did not like about their interview they had with their customer
*   Everyone did well
*   Don't be afraid to go up to people

# Week 3

*   Interviews
*   Business Partners
*   What they want
*   What they know about Wintec
*   Students
*   They experience in spaces (Lecture experience, assignments experience, personal experience)

# Week 4

*   Breakfast (16 August)
*   What was the points from the students you interviewed on your own
*   Empathy map
*   What was the person feeling, thinking
*   Interview with staff
*   Followed by a Empathy map
*   Personas
*   Create a persona (student and teacher) from the data we gathered from day 1
*   We have a name, we are now Prime Design.
*   Enter the ‘Define’ phase

# Week 5

*   Spark
*   Talk to students, staff and institute
*   Gained some knowledge from students of what the want and wish from the space
*   Mind maps based on the new knowledge
*   Talk with Lindra about the directions of wintec
*   Trigger questions, the pains and gains of learning spaces for students and teachers\
*   Removed questions we made at the start of the design factory
*   Personas from new knowledge

# Week 6

*   First Timeline
*   Bring up information from new interviews, added more insights
*   Two groups made 8 by 8 ideas of what a space should have, this will be used to consider what a challenge learning space will have.
*   Themes and insights
*   Ideated
*   Refined ideas
*   Prototyping
*   "Put ideas created into physical ideas"

# Week 7

*   Talked with Heith Sawyer
*   Teaching and learning coach
*   Did a design print
*   Gave insights
*   Incorporated Heiths insights into our insights
*   Timetable of what we will do this week to prepare for presentation
*   Heuristic ideation tools
*   Did storyboards, to plan for what our presentation will be about

# Week 8

*   Present to Executives
*   Did an activity with Executives of making a space with the ideas we presented

# Current ideas

## Environment

"U - Work"(Individual work areas): • Individual work areas. • quiet spaces. • variety of group work spaces.

"Move'em Out"(Flexible Furniture): • Configurable to the persons needs. • Can move for more available room • Collapsible • Variety of furniture ◦ Tall tables ◦ Small tables

## Technology

"Quick Roll"(Swipe card roll) • Use id cards to register students in the class • Start class sooner Teacher instantly knows who is there

Image 2 "Smart Table"(Touch screen+ Whiteboard): • Touchscreen table. • white board. • raisable to present to class. • share ideas with class mates.

“Two view” (mirror display): • tutor can send their screen to the students so students can follow along. • students can connect to a cow or big screen to show work or video on their phone.

## Teaching and learning approaches

"Rail Desk": • Rail which boards and screens can move around on. • Addable Attachments.

U Page(Moodle): • personalized LMS/Moodle. • Add your own links. • Adaptable Learning.

Images 3 "Pre-class"(Pre-settable class): • Preset class. • tutor swipes their card and the projector and tv posters load up with todays lessons and resources.

Image 4: Wintec TV(Recording class): • Mobile Learning • Allow continues Learning • Allow students to catch up

Image 4 "TV Posters/Digital Poster • Personalization • Wintec updates • visual aids

# Week 9

*   Begin creating physical prototypes.
*   Lotus Flower (Refine ideas, Create new ideas).

### Talk with the archaeologists.

*   Many similar ideas
*   Education institutes need to be self reflective
*   Not just tech fails the teacher can

# Week 10

### Organize Industry breakfast

Prepare for the breakfast on wednesday

# Week 11

*   Prepare for Breakfast
*   Refine Prototypes
*   Breakfast
*   Refine Prototypes
*   Start making a floor plan (Rail Desk)
*   Refine pre-class
*   Identify who and where we test prototypes

# Week 12

*   Working on prototypes

# Week 13

*   Statistics
*   User testing
*   Talk with jaine hill and sam (Choudhill)

# The future

## Week 14

*   Draft artifact
*   Preperation for Presentation

## Week 15

*   Presentation
*   Artefact

## Week 16

*   Gala