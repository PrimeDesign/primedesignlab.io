---

title: Week 2

subtitle: The Interviews Begin and a trip to the unknown

date: 2017-08-04 00:00:00 +0000

publishdate: 2017-08-03 00:00:00 +0000

tags: [
"Alex"
]

bigimg: [
  {
  src: "/images/Questions_1.jpg",
  desc: "Questions | Ways of Learning"
  },
  {
  src: "/images/Questions_2.jpg",
  desc: "Questions | Future Skills"
  },
  {
  src: "/images/Questions_3.jpg",
  desc: "Questions | Technology"
  },
  {
  src: "/images/Questions_4.jpg",
  desc: "Questions | The Way We Use Space"
  },
  {
  src: "/images/Questions_5.jpg",
  desc: "Questions | Social Change"
  },
  {
  src: "/images/Meeting.jpg",
  desc: "Meeting"
  },
  {
  src: "/images/Portraits.jpg",
  desc: "Portraits"
  }
]

---
# Day 1 (2 August)

* Members missed previous meeting caught up
  * [Portraits](/images/Portraits.jpg)
  * [Teamwork Questionnaire](/pdf/Teamwork_Questionnaire.pdf)
* Create Questions on multiple boards to understand the Issue

The goal of this is to break down the issue we have been given into possible other issues to try understand what this issue entails

* Introductions to both project members and Clients
* Clients contributed questions and answered some of the questions.

Gain an understanding of what the clients want along with being a rapport with the staff

* Preparations for interview
  * Interview Theory
  * Prepare questions
* Interviewed Staff
  * Me and Hayley interviewed a teacher, Others answered another Teacher and a staff at Wintec
* Conclusions
  * Project members did a group activity about what they liked and did not like about their interview they had with their customer
  * Everyone did well
  * Don't be afraid to go up to people

## Day 2 (4 August)

* Looked around Rotokauri campus
  * Looked at learning space
  * Looked at lectures
  * Looked at workshops
* Asked some members (Staff and students) what they think of learning spaces
* Two members missing
* One member face timed

To give team members an example of how wintec/industry are making learning spaces.

Find out more about what we find:
[Learning Spaces - Wintec](/post/learning-space/)

## Powerpoint

[Week 2 Notes](/pdf/Week_2_Notes.pdf)

## Upcoming for next week

* Meeting with the customers (IT and resources) will hopefully discover more on the project