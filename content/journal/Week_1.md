---

title: Week 1

subtitle: The Beginning

date: 2017-07-28 00:00:00 +0000

publishdate: 2017-07-28 00:00:00 +0000

tags: [ "Alex" ]

bigimg: [{ src: "/images/challenge.jpg", desc: "Challenge", }, { src: "/images/process.jpg", desc: "Process" }]

---

# Day 1 (26 July)

Today we discovered:

*   What the problem/issue we are facing
*   Who the team members are

The purpose of this is to get the team to open up to as much as they are comfortable with and for the team to be introduced to the problem

*   Did some team building
*   Brainstorm a start up focused around coffee
*   Coffee Delivery
*   Coffee Pickup
*   Create a figure based on a member of the team you meet on there things the member told you about them

The team worked together on a problem to

*   IT talked about the mini project
*   In my case the website

The goal of the website is to detail journals and the journey through the design hub

## Day 2 (28 July)

Today we discovered:

*   What we have to prepare for
*   Brainstormed questions related to the issue of the project
*   Each member selected what question they wanted to research (to present in a later class)

Give people a task to do outside of the design hub

*   Did some team building
*   2 Truths and 1 lie
*   Create an image based on yourself
*   Did a personality test to discover what quality you most fit

Get people to open up more

*   Three members were missing some because of work conflicts and others getting sick

## Powerpoint

[Week 1 Notes](/pdf/Week_1_Notes.pdf)

## Upcoming for next week

*   Meeting with the customers (IT and resources) will hopefully discover more on the project