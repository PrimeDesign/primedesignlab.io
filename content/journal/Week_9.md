---

title: Week 9

subtitle: The protypes and archaeologists

date: 2017-09-28 00:00:00 +0000

publishdate: 2017-09-28 00:00:00 +0000

tags: [ "Alex" ]

---

# Day 1 (27/09)

Activities:

*   Loosen Up.
*   Create physical prototypes.
*   Lotus Flower (Refine ideas, Create new ideas).

## Day 2 (29/09)

### Talk with the archaeologists.

*   Many similar ideas
*   Education institutes need to be self reflective
*   Not just tech fails the teacher can

## Upcoming for next week

*   Perhaps do activity on website homepage re-design
*   Find out if people need help on editor