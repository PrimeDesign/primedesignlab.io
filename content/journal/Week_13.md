---

title: "Week 13"

subtitle: ""

date: "2017-10-29"

publishdate: "2017-10-25"

tableofcontents: false

tags: [ "Alex" ]

---

## Day 1 (24⁄10)

*   Work on prototypes
*   User testing

## Day 2 (25⁄11)

*   User testing
*   Talk with Jaine hill and Sam (Choudhill)

## Day 3 (27⁄11)

*   Work on prototypes