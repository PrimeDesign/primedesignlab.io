---

title: 'Help Page'

type: page

---

![Editor Image](/images/Help.JPG)

## Getting Started

* Download Markdown editor and Site

https://gitlab.com/PrimeDesign/markdown-editor/tags

### For publishing capabilities

* Install Git
https://git-scm.com/

** Mac **
can type 'git' in the terminal or go to git-scm


## Open File

* Direct Editor to the file
(All Content for the site is in the content folder)
(e.g. primedesign.gitlab.io\content)

## New File

1. Pick a place to save file
2. Name File
3. A template will be added
4. You can now start writing

## Save File

1. Click 'Save File'
2. File will now be shown

*Note*
Files will be saved automatically

## Write Content

### Headers

1. Select the line you want to be a header
2. Select the dropdown
3. Select what header you want

### Emphasis

#### Bold

1. Select the line you want bold
2. Press the Bold 'B' symbol on the toolbar

### Lists

1. Select the line you want to be a list
2. Press the list icon on the toolbar

### Links

1. Select the line you want to be a link
2. Press the link icon on the toolbar
3. Popup will appear

### Images

Currently can add images to the editor but cant publish them

## Save

1. Click save button
2. File Saved
3. Along with a prompt so you know its saved

## Publish

** Publishing requires git **

1. Simply hit the publish button
2. In about 30 sec content will be added to site