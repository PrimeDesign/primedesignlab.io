---

title: "Design Factory"

subtitle: ""

date: "2017-10-25"

publishdate: "2017-10-25"

---

## Introduction

Design Factory NZ is a learning space in wintec that supports innovation and creative thinking using a design thinking process. Students from multiple disciplines join the design factory to solve the wicked problem.

## Wicked Problems

It’s a complex, difficult to solve, time-consuming problem. These problems are typically problems that; dont have a simple solution, may not have the time or resources to solve. These problems can result in if solved reduce waste, save costs, things within the business to run more smoothly.