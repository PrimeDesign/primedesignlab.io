---

subtitle: Who is this great man

title: About the developer

type: default

publishdate: 2017-08-04 06:26:29 +0000

date: 2017-08-04 06:26:35 +0000

tags: []

bigimg: []

tableofcontents: false

---

My name is Alex. I have the following qualities:

*   Apparently leader and Doer (Blame the quiz)
*   Love Gaming
*   Hats are cool
*   Logical
*   Social awkwardness
*   Bad Handwriting
*   Night Person
*   INTP

## Skill Sets

### COMPUTER CONTROL PROGRAMMING / OPERATIONS

*   Computer Programming
*   Maintenance

### PROGRAMMING SKILLS / TECHNOLOGY

*   Application Programming
*   Database Programming

### PROGRAMMING & ASSEMBLY LANGUAGES

*   C#
*   Java
*   Structured Query Language (SQL)
*   PHP
*   HTML
*   CSS

### WORD PROCESSING / PRESENTATION / DATABASE PROGRAMS

*   Microsoft Office Suite

### GRAPHIC DESIGN / DRAWING / PAINTING / DIGITAL IMAGING

*   Adobe Photoshop

### Web Development

1.  Demonstrate an understanding of the legal issues affecting multimedia application development and determine their implications on development:

*   Examine and discuss various legal issues such as the Copyright Act, trade marks, patents
*   Examine and discuss small case studies which affect multimedia developers, e.g. legal issues, government legislation

1.  Demonstrate a clear understanding of the theoretical aspects of the key elements of digital multimedia: graphics, audio, video, and animation:

*   Examine graphics, video and audio data representations
*   Examine different approaches to compression
*   Compare and contrast multimedia compression techniques
*   Examine video and audio standards and their application
*   Examine approaches to animation development

1.  Design and develop multimedia applications which incorporate graphics, audio, animation, video

*   Sample and convert analog audio into appropriate digital format
*   Design and manipulate graphics, using appropriate formats
*   Design and develop animation using various techniques, e.g. GIF format, macromedia flash

1.  Create and program multimedia websites that employ various client side scripting techniques

*   Examine and discuss various versions of the document object model
*   Compare and differentiate web browser characteristics
*   Examine common scripting techniques, such as browser sniffing
*   Implement various types of script-based user interactivity

1.  Devise and develop a basic design strategy for multimedia applications

*   Examine basic web design issues e.g. site design, page design, content design
*   Discuss and apply web standards to a case study
*   Examine examples of effective/ineffective design

### Game Development

*   Engage in participatory teamwork
*   Practice Software engineering principles
*   Apply formal testing & documentation procedures
*   Discuss business processes and practices

Implement games directed software

*   Develop animated graphics
*   Produce and enable automatic demonstrations
*   Design and construct interactive games
*   Use programming languages and software elements that support games programming
*   Proficiency in programming languages enhanced (for example C++)
*   Effectively use Standard Libraries

Research existing and emerging games technologies

*   Evaluate the progression of software and hardware technologies used for games
*   Evaluate the progression in gaming theories supporting popular computer games
*   Investigate the role of artificial intelligence (AI) in games

Design and build for the real-time gaming environment

Use libraries such as OpenGL, and/or DirectX (or similar) graphics to support games implementations

### Mobile Development

*   Investigate issues in Mobile Devices and Mobile Applications
*   Rationalize and analyse the concept of ‘Mobile Applications’
*   Discuss design heuristics and user interface designs for mobile devices and mobile applications
*   Investigate a design methodology
*   Investigate RAD Tools and Multi-Platform development
*   Explore existing and emerging developments in mobile devices and mobile applications (for example: web-based applications, GPS or games development)

Discuss and apply the mobile applications development process to a given platform

*   Discuss formal methods and design principles for development of a mobile application
*   Design a mobile application
*   Implement the mobile application
*   Test the mobile application for verification
*   Discuss business processes and strategies for deployment of a mobile application

Implement a mobile application using industry based methods and software for the given platform

*   Use a variety of GUI components
*   Apply suitable layout structures
*   Include resource files (for example: graphic images, media files)
*   Include multiple screen implementations
*   Serialise preferences and settings
*   Apply portrait and landscape screen display orientations

Use application language features in the given platform

*   Demonstrate proficiency in the programming language choice (for example: Java, XML, XCode, C#, Objective C, .NET or the programming language used in the RAD Tool)
*   Effectively use standard libraries and features for mobile applications

## Responsibilities

*   Prime Design Website

## Qualifications

*   CCNA Routing and Switching: Introduction to Networks(2015)
*   CCNA Routing and Switching: Routing and Switching Essentials(2016)
*   MJW IT Essentials: PC Hardware and Software (2017)
*   PRINCE2® Foundation Certificate in Project Management(2017)

## Experiences

### Business Support

Fire Service

Kevin Holmes

October 2016 - July 2017

### Gardener

Frankton Kindergarten

October 2013 - October 2014