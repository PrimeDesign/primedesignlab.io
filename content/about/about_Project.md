---

subtitle: What is the project

title: About

type: default

publishdate: 2017-08-04 06:26:29 +0000

date: 2017-08-04 06:26:35 +0000

tags: []

bigimg: []

tableofcontents: false

---

Design Hub is about trying to solve the issue "New learning spaces that integrate technology with new learning approaches to best prepare students for work life"