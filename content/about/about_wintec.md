---

title: "Wintec"

subtitle: ""

date: "2017-10-25"

publishdate: "2017-10-25"

tableofcontents: false

---

## Introduction

*   Wintec is a learning institute
*   92 years of
*   2,200 students eligible to graduate in 2016 with qualifications in the creative industries, health, information technology, languages, primary industries, science, education, business, sport and exercise science, hospitality, trades and engineering. (2016 annual report )

## Values

(snippet form wintec site https://www.wintec.ac.nz/about-wintec/who-we-are/our-values)

### Working together - Mahi tahi

We work collaboratively within and outside our organization. We form partnerships, openly communicate, share expertise and try new things.

### Challenge and Innovation - Whakaaro whānui

We are leaders, so we challenge ourselves and others to look for ways to do things better and to embrace innovation and achievement.

### Customer Focus - Manaaki tangata

Students and employers are our customers, along with colleagues in the organization. We drive our organization from their needs, and act with purpose, creativity and energy to exceed their expectations.

### Valuing People - Whakamana i te tangata

We treat everyone with courtesy and respect, without prejudice and valuing different perspectives. We involve and listen to others, and recognize them for their contribution; always acting with integrity.

### Taking Ownership - Kia tika

We are all responsible for the overall success of our organization, and are accountable for our actions and results. We make quality decisions based on sound information and we learn from our mistakes in a 'no blame' culture.

### Improvement and Opportunity - Kia tupu, kia hua

We are committed to setting high standards and continually improving what we do. We are passionate about extending opportunities to students, employers and the wider community.