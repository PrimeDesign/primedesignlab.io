---

title: Mobile Learning?

subtitle: Learn on the go

date: 2017-08-30 00:00:00 +0000

publishdate: 2017-08-30 00:00:00 +0000

tableofcontents: true

tags: [ "Alex", "Mobile Learning" ]

---

## Toward learner centered education

### Mobile Learning definition

2005

O malley

> Any sort of learning that happens when the learner is not at a fixed, predetermined beaten, or the learner takes advantage of mobile technology

Troxler

> Education where the sole or dominant technologies are handheld devices

This points out mobile learning uses technology and the learning is not done in a fixed place

2007

Sharples et al

> The process of coming to know through conversations across multiple contexts amongst people and personal interactive technologies

This statement is more focused on having a conversation, an oral exchange of sentiments, observations. THis also could mean mobile learning requires verbal communications

## Discover in learning 1970

Bruners (1966) He believed students are more likely to remember concepts they deduce on their own

Along with believed technology in schools was behind

## Constructivist learning 1980

The development and distributution of multimedia personal computers offered such as interactive learning

(Naismith, Lonsdale, Vavouyla & sharples, 2004)

> "The computer was no longer a conduit for the presentation of information. It was a tool for the active manipulation of that information"

## Constructionist Learning 1980

Students learn when actively involved in constructing social objects

Papert, 1980 THe computer as he tutor approach would involve students using Logo to teach the computer to draw a picture

## Problem based learning 1990

Originally made in medical education in the 1950

Problem based learning involves students working on tasks and activities to the environment in which their skills are tested on

Tech was hard to move at this time

Students then learn by contracting their own knowledge form thinking critically and creatively to solve problems

Mobility became a desired attribute for technologies

LEarner centered education focuses on the role of the learner rather that the teacher

Teacher is the guide in the process rather that the teacher being the source of the knowledge

Students worked in groups of five or six

## Socio-constructivist learning

Knowledge is co-constructed interdependently between the social and the individual

Interaction with a group

## Advances in technology

### 1970

Technology came around like:

* Floppy disks
* Microcomputers
* VHS
* First mobile phone

Kay(1972) Create a concept model of the Dynabook, a portable computer

The Dynabook never left concept stage because the tech was not advancing fast enough

First mobile phone developed in 1973 was the motorola Dynatac 800X

### 1980

* More companies get involved in the handheld market
* These devices were resembling the Dynabook
* The first commercial laptop computer was introduced to the market
* Late 1980 and 1990s many schools, colleges began to allow students to bring laptops into schools
* Cell phones began to be more customizable, flexible and smaller

### 1990

During this decade, the learner centered movement is happening

* More devices came in:
* Digital camera
* Web browser
* Graphics calculator
* Palm pilot was also made, this device is a personal digital assistance. This device ran basic programs, test, calculation, calender, contacts etc.
* Devices have decreased in size, cost,processing power, memory, storage and functionality
* Wireless networks came with wide band technologies, allowing for methods like e-learning

## Connecting the technologies and learning

### E-Learning

Early definitions describe e-learning as teaching and learning supported by electronic media and tools

Keegan (2002) E learning was distance learning

Tavangarian, Leypold, Nolting and Voigt (2004)

> All forms of electronic supported learning and teaching, which are procedural in character and aim to effect the construction of knowledge with reference to individual experience, practice and knowledge of the learning. Information and communication systems, wether networked or not, serve as specific media to implement the learning process.

The internet made a significant portion of information during the 1990s and 2000s

Websites have gone from static to dynamic and interactive, progressing from read-only web to read-write

In the past 20 years, a great many artefact such as books have been converted to be used for the web

What e-learning lacked early on were physical interactions with the environment and society, without spatial and temporal limitations

### M-Learning

Started by channeling e-learning methods and techniques

Tech at the start of m-learning wasn't advance to wasn't meets their demands

At the time PDA's were used, but because of tech like smartphones, the interest and development of PDA's decreased. Smartphones were considered because smartphones had the same functionality and had a higher social status.

M-Learning - 5 Characteristics

1. Continues learning

Allow learners to respond and react to the environment

2. Situated learning

Learning takes place in the surrounding appliance

3. Authentic learning

Tasks directly related to the immediate learning goals

4. Context aware learning

Learning is informed by the history and environment

5. Personalized living

Customized for each learner

M-Learning as a subfield of open and distance education

M-Learning has far greater potential learning because portable devices are getting cheaper and more accessible

## References

Berge, Z. L., Crompton, H., & Muilenburg, L. Y. (2013). Handbook of Mobile Learning. Taylor and Francis.