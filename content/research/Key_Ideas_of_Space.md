---

title: Key ideas of space?

subtitle: What are has happened to learning spaces

date: 2017-08-07 00:00:00 +0000

publishdate: 2017-08-07 00:00:00 +0000

tableofcontents: false

tags: [
"Alex",
"Learning Space"
]

---
# Space that speaks to us

Our library building should reflect our views about variety, choice and diversity by our beliefs and values about the importance of the library space.

## The third place

Oldenburg idea of the third place

"Third places are neither home nor work, the 'first two' places but are venues like coffee shops, bookstores and cafes in which we find less formal acquaintances. These comprise 'the heart of community's social vitality where people go for good company and living conversation'"

Libraries need a model that combines the personal of the first two and the productivity of the third place.

## References

Watson, L. (2014). Better Library and Learning Space. Facet Publishing.