---

title: What is the history of Learning Spaces in the US?

subtitle: What are has happened to learning spaces

date: 2017-08-05 00:00:00 +0000

publishdate: 2017-08-05 00:00:00 +0000

tableofcontents: true

tags: [
"Alex",
"Learning Space"
]

---
# US

## Introduction

* Successful libraries are transformation spaces.

* Libraries can be considered great eg architectures but may not be good.
  * Most consideration comes from critics, comes from designers. Not the occupants of the building.

* Exterior design will be considered secondary when it comes to consideration to the interior
  * The new york public library is considered great, even though people who go there really sit foot into the building.

* "The visitor should experience a sense of having steeped away from the pressures, distractions and often toxic energies from the outside world".

* All learning spaces should create an awareness of a special place, where learning takes place, where contemplation and thought is encouraged, where those curios can readily commune with the authors.

## Not all spaces are universal

* In the 1960's, Sommers, a environmental psychologists, conducted research on  how individuals interact with one another in the built environment, along with how students use study tables in libraries.

* What was discovered is men and women preserve spaces differently. Women interact with the people and objects they share space with in ways distinct from men and that furnishings, one size does not fit all.

### Hollins University

Formally a women-only liberal arts school

The preferred seating in the old Hollins library was the sofa, which was comfortable for study , conversation or napping

This piece of seating permitted the first occupant to control the space - inviting others to sit down

These piece of seating were usually situated against a wall or back-to-back with another sofa, permitted the occupant to be able to see who was around or approaching

It was the perfect solution for a specific population

Hollins had plenty of traditional library seating

This selection of the furnishings clearly reflected the preferences of Hollins students and facility

## Wells college Library

 Also a women's only school

Seeking to move beyond 'the boredom of the box'

The design of the college library,

### Students

* They did not like the oversized building supports

* They did not like the multi-level public spaces that presented challenges to those with mobility problems

* They did not like the low level levels of illumination or the lack of ability to adjust the lighting to match their needs

* They did not like the lack of seating options

* Group studies are minimal

* Most preferred to study in their dormitory rooms, the student dining facility or lounges

## Great library spaces can be an accident

## Trends

### The commons

Spaces IT resources and interior should be shared and rationed for visitors

### A resources at the centre

Resources are located in close proximity to the entrance.

### More flexibility

Always have the ability to change. Users are encouraged to move furniture to suit their personal or group needs

### More capacity

Be ready for future needs. TO anticipate increasing demands and assure optimal connectivity, new networks are designed that feature salability

### More support

visitors benefit from and appreciate having ready access to knowledge and technology-savvy staff, who can answer questions or provide suggestions that guarantee a successful visit

## References

Watson, L. (2014). Better Library and Learning Space. Facet Publishing.