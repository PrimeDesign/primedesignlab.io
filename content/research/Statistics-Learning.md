---

title: Statistics Learning

subtitle: 2016

date: 2017-09-12 00:00:00 +0000

publishdate: 2017-09-12 00:00:00 +0000

tags: [
"Alex",
"Learning Space"
]

---

## Student Study

46%of students say they get more
actively involved in courses that
use technology.

78% of students agree that the use of technology contributes to the successful completion of courses.

![Student Study](/images/Student-Study-Graph.jpg)

LEARNING ENVIRONMENT AND ACADEMIC EXPERIENCES:
82% of students prefer a blended learning environment.

6 in 10 students say they want their instructors to use these more:

* Lecture capture
* Early-alert systems
* Free, web-based supplemental content
* Search tools to find references/information online for class work

![Student Study](/images/Student-Study-Graph2.jpg)

Percentage of students who say that technology has helped them:

* Ask instructors questions 79%
* Engage in the learning process 71%
* Work with other students on class projects 69%
* Participate in group activities

2/3 of students typically connect two or more devices to the Internet at a time.

4/10
students say they get distracted in class by text messages, e-mail, social media, or web surfing.


## References

Student Study 2016 -  ECAR 