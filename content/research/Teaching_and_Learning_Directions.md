---

title: "Teaching and Learning Directions"

subtitle: "2017-2020"

date: 2017-08-23T13:41:19+12:00

publishdate: 2017-08-23T13:41:19+12:00

tableofcontents: true

tags:
- Undefined

bigimg:
- src: "/images/Image.jpg"
  desc: Name of Image

---


## Principles

* Learner-centered
* Authentic
* Inquiry-based

Inherent in these principles is the notion of Ako which refers to facilitating positive learning relationships through shared leadership between learner and facilitator


## Learner-Centred Approaches

A variety of educationsal programs, learning experiences, instructional approaches and academic-support strategies that are intended to address the distinct learning needs, intrests, aspirations or cultural backgrounds of individual students


![Glossary](/images/SCL-Glossary.jpg)

Five Characteristics of Learner-Centered approaches:

* Engaging students in the hard, messy work of learning
* Includeds explicit skill instruction
* Encourages students to reflect on their learning
* Motivates students byt giving them control over their learning
* Encourages Collaboration

Terry Doyle

> When having a learner centered teaching you must consider each new method with the questions "Will this teaching actions optimize my studetns opportunity to learn?"

John Biggs

In order for teachers to adapting to Learner centered teaching, techers need to be skilled in contemporary teaching and learning methods

## Authentic Learning

Authentic learning is an institute having a wide variety of educational and instructional techniques focused on connecting what students are taught to real world issues

Audrey Rule oulines four key characteristics:

* An activity that involves real-world problems and that mimics the work of professionals; the activity involves presentation of findings to audences beyond the classroom

* Use of open-ended inquiry, thinking skills and metacognition

* students engage in discourse and social learning in a community of learners

* Students direct their own learning in project work


At wintec, this can align to one of our values - Working together. Also meets with other culture backgrounds

## Inquiry-based Learning

IBL is a way of teaching which best enables students to experience the processes of knowledge creation

The key attributes are:

* Learning stimulated by inquiry
* A learner-centered approach
* A move to self-directed learning
* An active approach to learning



One of the advantages of IBL is that it attends to the students own intrests and cultural backgrounds so therefore reflects the co-constructed teaching and learning process. If the student is curious then you are following IBL.

## Putting All the principles Together

A key component underpinning all the principles and all the following teaching and learning approaches is the concept of 'active learning'

## Teaching and Learning Approaches

### Project based Learning



### Flipped Classrooms

