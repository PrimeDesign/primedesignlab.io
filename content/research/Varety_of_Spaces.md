---

title: Variety with balance

date: 2017-08-18 00:00:00 +0000

publishdate: 2017-08-18 00:00:00 +0000

tableofcontents: false

tags: [
"Alex",
"Learning Space"
]

---

## Balance

If information and learning are to support learners they need to create a variety of spaces that speak to the different types of people that are using the spaces.

Rizzo list fow different types of spaces:

* Highly active and engaging communal places
* Interactive collaborative places for individual research and group work
* Quieter less active places such as reading rooms, study rooms and alcoves
* Out of the way contemplative places for quiet reflection and deep thought

They are vague spaces but are mostly for starting points.

Balance of these types of study rooms varies from library to library.

Involves the consideration of the extent of the space.

Spaces should be able to switch from quiet to loud and the opposite etc.

A really successful library or learning space will have to have a balance between types of spaces that meet the needs of the communities that use it and will  have a dynamic balance that extends expectations and can moth over the annual cycle of use to closely match demand over time.

Least successful are the spaces that dont match user need or are unable to be adapted to changing short term demands.

To summaries, spaces needs to adapt for different types of of people, which is the type of learner they are, the department they specialize in, their needs. Spaces also need to remember to balance each different type of spaces they have. Spaces also need to be able to switch in a easy manner.

## Flow

