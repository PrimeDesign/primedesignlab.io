---

title: Future Ideas

subtitle: What are has happened to learning spaces

date: 2017-08-19 00:00:00 +0000

publishdate: 2017-08-19 00:00:00 +0000

tableofcontents: true

tags: [
"Alex"
]

---
## Staff versus machine

Recent services indicate interaction between staff and users and still important [1](http://libraries.pewinternet.org/2013/01/22/library-services/). THis because of practical assistance and because libraries have the beliefs of people helping people to providing social context, there is also no replacement to a welcoming smile. Even though libraries will go through automation, staff and user interaction will be an important part to library life and furniture and will be a continue need to support this interaction.

## Front of house minimized



## Managing your total library experience

## Flexibility

As libraries offer increasingly more diverse programs and as each space has furniture that is used for an intended purpose that often gets changed again for a new purpose.

## Tables and chairs

## Love to lounge

Lounge areas have become an expectation in todays library space. Typically these spaces are populated by various seating options such as tub chairs, sofas and ottomans, which are usually neighbors to coffee tables. Lounge areas encourage connective  social experience and practically in tertiary libraries, support informal learning interaction. Lounge chairs have not deviated to far from designs as far back as the 1930s which suggests they are unlikely to change to this day or not have a significant change. In today we have chairs with power outlets and chairs designed for gaming or as listening posts. In the future chairs may support 3D holographic interactions and experiences.

## My space your space

Space division is an important component in encouraging personal or group activities and achieving efficient space utilization.

## Displays

With the move toto digital, many physical library products will disappear including CDs, DVDs and a potentially a large percentage of books, newspapers and magazines. THeme displays, focus displays and face displays will continue top 'dress' library spaces, enriching the user experience. THus the familiar display stands will remain useful but products and services will increasingly be promoted via digital media.

## Kids are the future

Children areas are often the most create areas in a learning space, with things like; story time zones, holes for children to hide and play in, kiddy themed furniture and the inclusion of touch screen based activities. Kids love knowing they have their own special place, they love to roll around on the floor, to reach things and do things by themselves. Libraries can play a key role in developing children's knowledge and creativity. Exploring furniture designs that understand kids will greatly assist libraries developing kids.

## Learning spaces

As technology revolutionizes the way we learn exciting new ideas like massively open online courses are being trialed and models around like long learning pursued. It remains likely that getting together in groups will remain an important component of learning.

## Focus space

## Outside

Wether nether co-operates with people but many learning spaces include outside areas. These areas are populated with park benches and often supported by wifi coverage. There is great potential to make better use of these spaces with well designed outdoor furniture, which creatively address comfort, community and cover.

## References

Watson, L. (2014). Better Library and Learning Space. Facet Publishing.