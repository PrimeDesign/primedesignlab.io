---

title: Six thinking hats

subtitle: Edward de Bono

date: 2017-08-03 00:00:00 +0000

publishdate: 2017-08-03 00:00:00 +0000

tableofcontents: false

---
# Hats

## Definition

Six thinking hats is an invaluable teaching tool for thinking. It breaks down the thinking process into
six components. Each child in the class should experience all six levels of thinking. The six hats take
the ego out of thinking and allow more objective and comprehensive consideration of the issue.

## White hat

* Data and information
* Note all views even if
* Information conflicts
* Assess relevance of information
* Assess accuracy of information
* Separate fact from fiction

## Red hat

* identify feelings and emotions
* gut responses, intuition
* debriefing tool after a decision has been made
* no requirement to justify feelings

## Black Hat

* possible and existing negatives
* identify bad points
* judgement and assessment of a situation
* caution

## Yellow hat

* positive aspects of thinking
* benefits
* accept all suggestions even if not feasible

## Green hat

* new ideas and alternatives
* modifies existing ideas
* provides new directions
* good points
* reasons why an idea will work

## Blue hat

* keeps an overview
* manages the thinking process
* gets the best thinking from everyone
* usually the role of the facilitators
* focuses and refocuses thinking