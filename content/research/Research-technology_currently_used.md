---

title: How is technology currently being used?

subtitle: What are people currently doing with technology

date: 2017-07-31 00:00:00 +0000

publishdate: 2017-07-31 00:00:00 +0000

tableofcontents: true

tags: [
"Alex",
"Technology"
]

---
# Introduction

Over the years libraries have used technology to improve operational efficiency adopting systems to automate library functions and services that focus on the acquisition, management and circulation of resources, mor recently extending these activities, somewhat imperfectly, to digital resources. There is a real threat that such unpleasant environments, increased availability of digital resources, remote access, personal ownership of devices and the inadequate variety of the technology response that is the 'commons' will make visiting the physical library. The question becomes will technology kill libraries?

## Changes we expect - revolutions we don't

Seeing where technology is not easy, and spotting technology innovations are likely to become ubiquitous. How will driverless cars, Google glass, 3D printers, cloud computing, the social web, the web of things, wearable technology, robots, machine Intelligence affect the library. This incomplete list includes the technology we have now - the reality is that the continuing development of technology at a rate that is changing that is changing that is increasing exponentially.

## What is Technology

> Technology is anything that was invented after you were born Alan Kay(in Kelly,2010)

The question is not 'Will technology kill the library?' but 'What is the next form of the technology that is the library?'.  What is needed for a library to internalize and assimilate technology using imagination to take the technology that is the library to its next stage, rather than continue the current position in which libraries bolt on technology as they emerge. As it evolves it creates greater opportunity - specifically opportunities for human betterment through the extension of our intellectual and creative capability.

## Tactics to strategy

Bolt-on approach tend to adopt technologies only when safe to do so. For technology to be internalized and assimilated by any organization it has to be future focused and integral to the strategic framework of the organization.

```sequence
People->Environment
Environment->Technology
Technology-->People
```

 Technology in strategic context

Technology has to enhance the work of the people of the organization and to be integrated into the organizational environment.

While the strategic approach will not predict the future it does provide a clear framework for thinking about technology and specifically how it interacts with other factors within the technology system.

## Technology rich Library spaces today

Current technology-rich library space has ubiquitous networking both wired and wireless, enabling library members to use technology devices throughout the space

## Big Data

In 2013 Jisc launched the Jisc Library Analytics and Metrics Project in the UK, which was aimed at enabling libraries to capitalize in the opportunities of big data. This collection of data can allow for libraries to discover trends like who uses the library. Also could allow for real time information which could be the push that libraries need to make.

## Robots

At the present librarians are still picking individual books but with robots one day there could be a standalone mobile robot that can search shelves.

According to UK Foresight Horizon scanning centre (2012)
> The principle markets for service robotics include defense, healthcare, manufacturing, transport, energy, entertainment, education. Service robots have the potential to transform the competitiveness of service industries but there seems to be a lack of social acceptance robotics.

Arthur C. Clarke(1980)
> 'A Teacher that can be replace by a machine should be'

If there statements are considered against the 'Technology in strategic context' graph, People and environment are not considered and because of that the questions that rise from these are. What do the staff think of these technologies. Do these environment space want to change for these technologies.

## References

Watson, L. (2014). Better Library and Learning Space. Facet Publishing.
