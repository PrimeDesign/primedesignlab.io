---

title: We live in a conceptual age

subtitle: What are has happened to learning spaces

date: 2017-08-07 00:00:00 +0000

publishdate: 2017-08-07 00:00:00 +0000

tableofcontents: false

tags: [
"Alex"
]

---
# Conceptual age

Ken Robinson

> "Current systems of education were not designed to meet the challenges we now face. The were meet for the former age.

Continuing to educate using only on industrial approach is highly questionable

Book Whole New Mind

> Describes society's transition cover the past 200 years from an agricultural to an industrial age and eventually to an age of information and knowledge.

Much of what we currently read, determines how wee configure libraries and what we teach in our libraries relates to this information society and the methodologies we used to teach it are informed by the instructional approaches of the previous ages.

## References

Watson, L. (2014). Better Library and Learning Space. Facet Publishing.