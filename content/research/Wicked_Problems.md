---

title: Wicked Problems

subtitle: Edward de Bono

date: 2017-08-03 00:00:00 +0000

publishdate: 2017-08-03 00:00:00 +0000

---
# Wicked Problems

## Introduction

## Criteria for Wicked Problems

## Modelling Wicked Problems

## Example Problems

1. How do we manage the risk of natural hazards to critical infrastructure?

[https://www.infrarisk-fp7.eu/](https://www.infrarisk-fp7.eu/)
[https://www.youtube.com/watch?v=JqdJEwt0rhI](https://www.youtube.com/watch?v=JqdJEwt0rhI)

2. How do we develop an Urban Good Governance policy for a city like Auckland? (for more on this type of problem see UN-HABITAT)
3. New Zealand is rushing headlong via private industry, backed by MBIE, into developing a space going capability. This includes rocket development, launch and other services. Is this not something that must be planned and scoped in a more holistic manner by a dedicated space agency? The problems statement may be as follows: What is The Structure, Functions and Societal Benefits of a New Zealand Space Agency
4. During crisis situations (like a major earthquake in Wellington, or any other large Metropole, or a dam wall burst or a geothermal electricity well breach), events cascade and in the ensuing chaos lives and infrastructure are lost. How would we prepare more effectively for this using a better framework that includes society and government and NGOs in a proper structure to respond in these situations? [http://fortress-project.eu/](http://fortress-project.eu/)
5. How do we change the cycle of crime that includes resource plundering (poaching of endangered species), drug trafficking and trade in women and children? 

[researcharchive.wintec.ac.nz/5152/](researcharchive.wintec.ac.nz/5152/)

6. As the effects of climate change kick in, the scarcity of resources like water may drive a process of conflict that spans borders. Famine and poverty in nations that will be hit first, will increase. In other nations, especially in our Oceania region, will see sea level rise and inclement weather conditions driving poverty and famine. How do we go about understanding the conflict scenarios and what strategies can we follow to stay ahead of these changing scenarios?

## References

[Video](https://www.youtube.com/watch?v=HrWbicvDLPw)
[Wicked Problems - Tom Ritchey](https://learning.wintec.ac.nz/pluginfile.php/909012/mod_resource/content/1/Wicked%20Problems%20-%20easy%20intro.pdf)