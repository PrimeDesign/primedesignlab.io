---

title: What is the history of Learning Spaces in the UK?

subtitle: What are has happened to learning spaces

date: 2017-07-30 00:00:00 +0000

publishdate: 2017-07-30 00:00:00 +0000

tableofcontents: true

tags: [
"Alex",
"Learning Space"
]

---
# UK

## Introduction

* 13% reduction in income between 2008 to 2011/12(LSU 2013).
* Press and television report libraries have not changed much in the past 50 years.
* Between the birth of the world and 2003 there have been five exabytes of information.
* We now create that much every two days.
* People are reading more and more because of e-books and paper and the internet.
* The internet has lead to a massive increase of information.
* There is a unmet need for learning facilitation and support.
* Despite tough financial assistance there have been new library developments.
* Library development is mostly in universities (refurbishment and re-purposed spaces, new buildings).
* Stewart Brand, User spaces have to fit for purpose on their first day of opening, they must also be fit for an unknown future.
* Brand also writes predictions about the future are always wrong.
* There are no right answers, One problem may have many solutions.

## Ayr Campus

* 81 Million Building.
* August 2011.
* 1326m².

* The building have woodland and river views and other windows looking out over internal courtyards with white facades. These combined with the glass roofs gave the interior of the whole campus a light transparent feel.
* The entrance is a bridge and walls at the building showing each floor. Making the library feel an integral part of the overall campus facility.
* This transparency and resulting vicarious observation of others opens up the library and signals the range of environments provided on campus.
* The main entrance is clearly visible and welcoming.
* The library security gates light up the entrance inviting the visitor to enter.
* Low level furniture i the entrance area is used for casual individual breaks in study.
* The area has a relaxed feel, enchanted by warm wood tones and colors.
* The upper floor has the purpose of being the busies area eg presentations, group work
  * Groups of computers
  * Presentation spaces
  * Movable tables
  * Smart boards
  * Plasma Screens
* The lower level is a calmer area
  * Computers
  * Power outlets for people to use personal computers
  * Shelves of books
  * Information focused Plasma Screens
  * Creates a "light touch" that creates a calm atmosphere as unnecessary information does not flood the space

## The Forum - -University of Exeter

* 48 Million Building.

The previous library was a tired 1980s building that would have required a demolition and rebuilding it if it was to house addition services.

### Previously

* Outdated row of student shops.
* Was difficult, uninviting site with the library separated form other facilities.

### Challenge

Find a way to develop the whole site and create a central focus for the campus.

Two projects were considered:

* A library refurbishment.
* Form project, to develop an integrated home for services for students.

In the end both projects were merged into one project.

### Result

* Curved glass wall mall.
* Joining together the library and the grate hall of the university
* Houses services for student, information desk, IT help, careers zone, group study rooms,, 400 seat lecture theatre, technology-rich exploration labs and a large retail outlet and cafe laid out along a covered street that provides social space and an exhibition space, library

The co-location of these facilities has been designed in such a way that, although each functional area had differing requirements for factors such as noise levels, privacy and group size, they can all operate successfully.

The library has benefitted from this project being part of that vibrant heart of the community and yet retaining those characteristics of the academic library that Exeter students value highly.

#### Library

* Formal and informal spaces to study
* Floor 1, group study and meeting rooms

## The Hive

60 million dollar

* level 0 is for 'young people' housing social study, games, film and music

* level 4 contains special collections and journals and is a quiet study

## Trends

### Open Spaces

* Promises ongoing reconfiguration for the way the spaces are used, emergencies trends and what is required

* Endless possibility

* Flexibility for the future

* Allows opportunity to regularly 'redesign' the space (New furniture, rearranging existing furniture,creating quiet and noisy spaces)

### Technology Rich Spaces

* Wired and wireless infrastructure, allows support for use of computers and portable devices

* Shared use of interactive boards in open pods

* Some cases loaned laptops

* Bringing own devices

* Current music and video technology

* Devices access to the provision of high quality network and data access

### Service Integration

* Bring services into one area (There is no separation between each service, library, shops, cafe)

### Develop learning communities

* Libraries allowed for services that would allow students to have continued education to prepare for the future industry trends.

## References

Watson, L. (2014). Better Library and Learning Space. Facet Publishing.