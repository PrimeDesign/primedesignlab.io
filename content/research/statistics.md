---

title: Statistics

date: 2017-10-29 00:00:00 +0000

publishdate: 2017-10-29 00:00:00 +0000

tableofcontents: false

chart:
    label: "% of Students and Teachers"
    type: "bar"
    labels:
        - "Practical Classes"
        - "Technology is unreliable"
        - "Comfort"
        - "Class Preparation"
        - "Recorded Classes"
        - "Open Plan"
    data:
        - "60"
        - "56"
        - "34"
        - "34"
        - "21"
        - "33"
    color:
        - "rgba(244, 66, 66, 0.8)"
        - "rgba(33, 150, 243, 0.8)"
        - "rgba(76, 175, 80, 0.8)"
        - "rgba(255, 152, 0, 0.8)"
        - "rgba(103, 58, 183, 0.8)"
        - "rgba(0, 188, 212, 0.8)"

---

<div id="chart"></div>