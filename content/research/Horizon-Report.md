---

title: Horizon Report

subtitle: 2017

date: 2017-09-12 00:00:00 +0000

publishdate: 2017-09-12 00:00:00 +0000

tags: [
"Alex",
"Learning Space"
]

---

Horizon Report believes that by:

* The next one to two years
  * Blended learning
  * Collaborative Learning
  * Adaptive Learning Technologies
  * Mobile Learning Technologies

* The next Three to Five years
  * Growing Focus on Measuring Learning
  * Redesigning Learning Spaces
  * The Internet of Things Technologies
  * Next-Generation LMS Technologies

* The next Five and greater years
  * Advancing Cultures of Innovation
  * Deeper Learning Approaches
  * Artificial Intelligence Technologies
  * Natural User Interfaces Technologies

## Significant Challenges Impeding The Adoption For Higher Education Technology

Solvable | Those that we understand and know how to solve

* Improving Digital Literacy
* Integrating Formal and Informal Learning

Difficult | Those that we understand but for which Solutions are elusive

* Achievement Gap
* Advancing Digital Equity

Wicked | Those that are complex to even define, much less address

* Managing Knowledge Obsolescence
* Rethinking the Roles of Educators

## References

Horizon Report | 2017 Higher Education Edition | NMC