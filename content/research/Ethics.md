---

title: Wintec Research Ethics

subtitle: What do you believe

date: 2017-08-03 00:00:00 +0000

publishdate: 2017-08-03 00:00:00 +0000

---
# Ethics

Preliminary: Applicant completes the Wintec [Ethics Screening Questionnaire](https://aewintecsitefinity.blob.core.windows.net/sitefinity-storage/docs/default-source/industry-community-documents/research/wintec-ethics-screening-questionnaire-updated-ma-26-09-16-gd.pdf?sfvrsn=5b79f733_2), in consultation with their Research Supervisor and/or Research Leader. This helps them decide which application they should complete and submit.

## Full Application

…where there are any significant concerns or potential risks to participants or the researcher. The Full Application Form includes detailed questions regarding:

Project details
Treaty of Waitangi
Other cultural considerations
Informed consent
Privacy
Data Storage
Form is signed by the researcher and Research Leader

## Low Risk Application

…where the nature of the potential/ actual risk of harm to participants or the researcher is minimal and no more than is normally encountered in daily life

The Low Risk Application form is signed by the researcher and Research Leader

THEN

Submitted to the Research Office at any time for review in consultation with the HERG chairperson

THEN

No HERG meeting required

The chairperson gives final approval and signs off on the Low Risk Application

## Additional Applications

Within Wintec

Institutional Consent…where the participants are Wintec staff or Students 

The Institutional Consent form is signed by the researcher and Research Leader Submitted to the Research Office for onward submission to the Dean for final approval

Outside Wintec

Health and Disability Ethics Committee approval is required for research involving:

Human participants as health or disability service consumers and their relatives or caregivers
Volunteers in clinical trials
Human tissue
Health information.
HDEC applications are completed online

Others, e.g. consent from companies, organizations or institutions to interview staff, members or students

## References

[https://www.wintec.ac.nz/industry-and-community/research-and-innovation-at-wintec/ethics](https://www.wintec.ac.nz/industry-and-community/research-and-innovation-at-wintec/ethics)