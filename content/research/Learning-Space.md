---

title: Learning spaces at wintec

subtitle: What does wintec think is good spaces

date: 2017-08-04 00:00:00 +0000

publishdate: 2017-08-04 00:00:00 +0000

tableofcontents: true

tags: [
"Alex",
"Learning Space"
]

bigimg: [ 
  {
    src: "/images/BasketBall_Court.jpg",
    desc: "BasketBall Court"
  },
  {
    src: "/images/Block_Entrance.jpg",
    desc: "Block Entrance"
  },
  {
    src: "/images/Cafe.jpg",
    desc: "Cafe"
  },
  {
    src: "/images/Court_Yard.jpg",
    desc: "Court Yard"
  },
  {
    src: "/images/Emergency_Exit.jpg",
    desc: "Emergency Exit"
  },
  {
    src: "/images/Group_Furniture.jpg",
    desc: "Group_Furniture"
  },
  {
    src: "/images/Hall.jpg",
    desc: "Hall"
  },
  {
    src: "/images/Kitchen.jpg",
    desc: "Kitchen"
  },
  {
    src: "/images/Lecture.jpg",
    desc: "Lecture"
  },
  {
    src: "/images/Lecture_2.jpg",
    desc: "Lecture"
  },
  {
    src: "/images/Library.jpg",
    desc: "Library"
  },
  {
    src: "/images/Personal_Spaces.jpg",
    desc: "Personal Spaces"
  },
  {
    src: "/images/Pods.jpg",
    desc: Pods"
  },
  {
    src: "/images/Safety.jpg",
    desc: Safety"
  },
  {
    src: "/images/Side_Entrance.jpg",
    desc: Side Entrance"
  },
  {
    src: "/images/Workshop.jpg",
    desc: Workshop"
  }
]

---
# First Looks (Observations)

## Pods

* Students Love them
  * But doesn't like moving from computer to pod

## Spaces

* Space is important
* TV's
  * To use TV's an hdmi cable is needed to be checked at reception
* Unknown how to connect
* White board

* Types of spaces
  * Social spaces
  * Spaces where you can be alone
  * Students spaces
  * meeting spaces
  * Learning studios
* Space is open
* Place to study

* Floor one is workshops, students
* Floor two is for meetings, teachers
* Stairs and elevators to higher floors

* Student simply come into the space for computer use
* Pods in each area of the space
* Power outlets across the rooms
* Furniture gets moved around for events eg open day
* Split rooms to accommodate for more groups
* Furniture flexible for easy movement
* Toilets
* Support areas (Students/Visitors getting assistance on issues they have)

## Complaints

* Population issue
* Classroom crowded
* Tutor needs to be loud speaker
* Difficulty one to one

## Likes

* Quite less distraction

## Research

Learning styles

## Students

* Some students prefer a distraction
  * Music, movie etc

* Some students preference have changed (Distraction to silence)
* Cater to. Multiple spaces
* Students move when going to lectures

* Farming area with cows (land survey)
* To allow the students to be more practical

## Teachers

* Teacher swap between whiteboard and projector and white over projection

## Navigation assistance

* How do students know how to use the space
  * Signs

## Resetting Rooms

* People who use the room reset put back when you entered the room
* Pictures
* Signs

## Comparisons

* Building in here is more colorful
* Why choose this colour palette
* Buildings in the city have windows but have curtains that cover the windows
* Each zones have their own differences

## Technology

* COW (Computer On Wheels)
* Lights
* Security cameras
* Smoke detection
* Loaned laptop's
* Wireless keyboards
* Sign in and out laptop for visitors
* Projectors
* Desktops
* Tablets
  * Dedicated to timetable of that room
  * Reserving rooms
* Plasmas screens
  * Dedicated to information. Timetable, announcements, map

Students like an always open space time and dimensions

## People to possibly interview

* Miko
* IT
* Project used user(students)
* Question Mike engineer

## Notes

* Announce people by title (typical student, person who has worked in projects)
* Think about the places you learnt the most and the places you worked the best
* These buildings are 3-4 years and considered new
* Ones in city are about 10 years and it feels outdated
* New changes had lack of information left people unsure their roles and their jobs
* Little issues can create one big issue
* Designers will focus on the look
* Engineers will focus on features
  * Steel can create interference (Wireless signals)
* If considering glass whiteboards
  * Consider tinted whiteboards, Students more preferred it

## Issues

* In this hub, there is multiple types of chairs and tables. In the city, just one type of table and one chair
* Teachers can't move to certain students in lectures, mostly students in the middle
* Students don't like not having a way to know if available computers
* Limited budget
* Students and teachers dont think there is enough whiteboards
* Would like one large white boards across the wall
* Students being childish, changing keys on a keyboard, moving keyboards in unexpected places
* Team not cleaning up after events
  * Potentially makes the room uninviting
  * Some solutions have been who used the room/area cleans it but that is not quite working