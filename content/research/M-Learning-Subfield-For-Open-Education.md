---

title: M-Learning being a subfield for open and distance education?

subtitle: Learn on the go

date: 2017-09-30 00:00:00 +0000

publishdate: 2017-09-30 00:00:00 +0000

tableofcontents: true

tags: [ "Alex", "Mobile Learning" ]

---

## Untied Arms

Comptar(2013)
> The definition of m-learning is likely to evolve, just as the definition of distance education has through out the decades

Course design also varies from system to system and ranges widely in incorporating course teams

Saba(2005)
> The field of distance education is fractured because of education cultures (premodern faculty, modern admintraction and post modern distance educators) attempting to evolve in the technology driven academic environment

There is also conceptual confusion, as new terms arise, new technology is made to combat it (e-learning, m-learning,distributed learning, blended learning, etc.)

## Social and technological Foundations of Distance Education

### Early Social Movements and the Opening of Educational Access



### Early Technologies and the Opening of Educational Access

### Lyceums and the Extension Movement

### The Emergence of Radio

### Educational Television

## References

Berge, Z. L., Crompton, H., & Muilenburg, L. Y. (2013). Handbook of Mobile Learning. Taylor and Francis.