---

title: Test markdown

subtitle: Each post also has a subtitle

date: 2016-02-20 00:00:00 +0000

publishdate: 2016-02-20 00:00:00 +0000

tags: [
"markdown"
]
---
# Markdown

## Here is a secondary heading

------

Here's a useless table:

| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
|col 3 is      | right-aligned | Four |
| col 2 is      | centered  | Nine |
| Seven | Eight | Six |
| Two | Three | One |

How about a yummy crepe?

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

Here's a code chunk with syntax highlighting:

```javascript
var foo = function(x) {
  return(x + 5);
}
foo(3)
```

## Block quotes

> Block quotes are very handy in email to emulate reply text.
> This line is part of the same quote.

Quote break.

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a block quote.

## Horizontal Rule

Three or more...

---

Hyphens

***

Asterisks

___

Underscores

## Emphasis

### Bold

For emphasizing a snippet of text with a heavier font-weight.

The following snippet of text is rendered as bold text.

**rendered as bold text**

### Italics

The following snippet of text is rendered as italicized text.

_rendered as italicized text_

### Strike through

~~Strike through this text.~~

## Youtube

{{< youtube b9KIRPA8aNs >}}

## vimeo

{{< vimeo 146022717 >}}

## Speakerdeck/PowerPoint

{{< speakerdeck 5c499bf3c2fe44ae87fa899e227be7a3 >}}

## Emoji

:bowtie:
:smile:
:sleeping:
:worried:
:frowning:
:anguished:
:open_mouth:
:grimacing:
:confused:
:hushed:
:expressionless:
:unamused:
:sweat_smile:
:sweat:

## HTML Tags

You can add html tags if you want

<iframe width="760px" height="500px" src="https://sway.com/s/palKrn5ZtOVJbFeq/embed" frameborder="0" marginheight="0" marginwidth="0" max-width="100%" sandbox="allow-forms allow-modals allow-orientation-lock allow-popups allow-same-origin allow-scripts" scrolling="no" style="border: none; max-width: 100%; max-height: 100vh" allowfullscreen mozallowfullscreen msallowfullscreen webkitallowfullscreen />