---

title: "Two View"

subtitle: "Mirror Display"

date: "2017-10-10"

publishdate: "2017-10-10"

tableofcontents: false

tags: [ "Prototype" ]

---

# Teaching

·      I wonder if it can be used for English language teaching

·      I wonder if you will lose the tutor to student interaction

·      I wonder about interactivity

·      I like Its like visual note taking

# Devices

·      I wonder how it connects

·      I wonder about storage

·      I like that this gives a specific purpose to devices

·      I wonder how Siri can help

·      I wish it could bridge the digital divide

·      I wonder how you deal with the complexity of varying devices

·      I wish it was fail proof & Inclusive

·      I wish it included all devices

# Implementation

·      Gives people an excuse to not pay attention

·      People with uncommon phones

·      I wonder if it will dis place the groups focus

·      I wonder how it can grab those people who can’t make it

·      I wonder how time would work with group work

·      I wonder how this might work in conjunction with other prototypes

·      I like the ability for students to choose and record

·      I wonder how this can be integrated with the present

·      I like how there are more options for groups etc