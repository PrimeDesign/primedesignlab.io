---

title: Brad

subtitle: Meeting

date: 2017-08-09 00:00:00 +0000

publishdate: 2017-08-02 00:00:00 +0000

tableofcontents: false

tags: [ "Meeting" ]

---

# Brad

## Meting 1

*   Wants students perspective
*   Wants the space to be used by students both inside the space and outside
*   Wants health and safety
*   Wants the space to be flexible and always have the possibility for change

## Meeting 2

### Issues

*   Can not always increase fees

### Funding

*   Funding is not limited to technology
*   "Where can technology take us"

### Micro credential

"Who wants to spend years"

*   Certified by wintec

### Government

*   Approve programs
*   Suggest programs
*   "Is wintec spending the money wisely"
*   "Government make it as difficult as possible"