---

title: IT

subtitle: IT, Human Centered

date: 2017-08-28 00:00:00 +0000

publishdate: 2017-08-28 00:00:00 +0000

tableofcontents: false

tags: [
"Meeting"
]

---

## Moodle

* Thinks students weren't considered in the making
* Students and teachers dont have the time to use or effectively use Moodle
* Teachers may not know how to use Moodle
* Many plugins for different purposes

## Moodle More Useable

* Open blank piece of paper
* Get requirements from the human (student, teacher)
* Then begin building

## The Hub

* The hub has good energy
* People meet up
* Project work is done there
* Like to know who and how many use the computers at the hub
* Wonders if upstairs is being used by students
* Thinks students weren't considered with the making.


* Could be bigger
* Having walls feel limiting
  * Could motivate more team work learning
  * Could allow for more space to be used

## Project Learning

* Business comes to IT
* Business come in because existing systems dont work how they want them to
* User is at the front of what it comes to when making decisions
* Ask the user
* Business potentially save money by taking the project to wintec and come back to the results of not to build the system
* Teach outside of class (eg moodle)

## Employees

* Employees are paying for workers to think not just do
* Workers are accountable for not working
* Will need to teach students not to hide
* Get to convince students to feel important and needed for the work life

## Classrooms

D Block
* Not good
* Bad with the balance between student and teacher
* When the class is after another class, there is no time to prepare for them
* Students can hide in the back of class
* Students have the tech to use (whiteboards) but dont use it because of the position of it.
* Time to set up for class needs to be shorter
* Teachers need to plan for tech to fail
* If tech fails some parts of the lesson have to be pushed back
* Teachers shouldnt have to
* Likes the design hub room
* Thinks teachers and dont use some tech effectively (using whiteboards like a projector)

C Block

* How i studied
* Students hide
* Made joke about bringing a jumper to hide

# Apps

* Does not use Wintecs apps/services
* Uses own apps
* Does work from mobile
* Wont know about wintec apps or other features unless told
* How will students know about stuff unless told about it