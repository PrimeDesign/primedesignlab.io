---

title: Gram

subtitle: Meeting

date: 2017-08-09 00:00:00 +0000

publishdate: 2017-08-09 00:00:00 +0000

tableofcontents: false

tags: [
"Meeting"
]

---
# Meeting

## Desired Outcome

* Effective environment
* Students are ready as possible
* Prepare for the work force
* Do not want students to be prepared for the idealogies of the past, lees effective ways of working
* The student to be the future for businesses
* Works in teams
* Creative Thinking
* An experience to be created
* "I dont want the space to be like a boring lecture, I want it like the design factory, Something that you dont knpow the outcome to"
* Relevant to the world
* Giving students confidence, get the students saying "I wasn't sure, i am now"
* The environment to be adaptable
* Prepare students for something, this space should give the students something

## Problems

* Problem is the challenge
* They are just a student, They are still developing
* Creating a learning experience, not just for student but also for staff and industry
* Getting the people involved to be comfortable
* "All jobs are different"
* "People do things differently"
* Interaction between student and staff
* Situational learning
* Getting industry involved
* "Why be here?" Why should or shouldnt students be there
* Whats the advantage of being here?
* Whats the role of being here?
