---

title: Margaret

subtitle: Meeting

date: 2017-08-02 00:00:00 +0000

publishdate: 2017-08-02 00:00:00 +0000

tags: [
"Meeting"
]

---
# Margret

* Transparency
* Wants the space to be used by student centered
* Wants the space to have industry involved
* Wants the space to have flipped learning
  * Not all classes will be in the learning space
* Wants the space to be project based
* The space needs to be equipped for learners will be at different levels
