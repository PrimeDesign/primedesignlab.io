---

title: "Smart Table"

subtitle: "Table made of tech"

date: "2017-10-11"

publishdate: "2017-10-11"

tableofcontents: false

tags: [ "Prototype" ]

---

## Practicality

*   I wonder how simple they can be
*   I wonder how heavy it is
*   I wonder if i can put my stuff on top it
*   I wonder about eating and drinking
*   I wonder how you can make it fail proof
*   I wonder how you can make it
*   I wonder how you might deal with stylus pen issue?
*   I wonder how you might protect the technology
*   I wonder about insurance
*   I wonder where people might put their belongings

## Tech

*   I wonder if it can be a projection
*   I wonder how you write on it
*   I like that a projection could be scalable
*   Print screen
*   I wish we can choose OS's back and forth
*   I wonder if its 4 screens or can become one
*   I wonder if the smart table connects to other screens in the room
*   I wonder if it is compatable with other OS?
*   I wonder if they can save what they do on the screen

## Teaching and learning

*   I wonder how this fits with teaching and learning directions
*   I wonder how this fits with teaching and learning idealogies
*   I like how students can work together
*   I wonder how tutors could also interact