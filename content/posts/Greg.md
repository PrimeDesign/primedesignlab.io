---

title: Greg

subtitle: Meeting

date: 2017-08-02 00:00:00 +0000

publishdate: 2017-08-02 00:00:00 +0000

tags: [
"Meeting"
]

---
# Greg

* Wants students perspective
* Wants the space to be used by students both inside the space and outside
* Wants health and safety
* Wants the space to be flexible and always have the possibility for change
