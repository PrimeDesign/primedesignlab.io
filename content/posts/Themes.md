---

title: Themes

subtitle: Meeting

date: 2017-09-08 00:00:00 +0000

publishdate: 2017-09-08 00:00:00 +0000

tableofcontents: false

tags: [
"Meeting"
]

---

## Environment

* Room temperature
* Light
* Color
* Sound (room)
* Space/ Distance between people
* Variety of objects (furniture)

## Teaching approaches

* Teachers need to know how to use tech and the space
* (Possible more tutors)
* Tutors are engaged when passionate
* Education training is beneficial

## Learning approaches

* Students prefer one to one learning
* (parking *wink*)
* Staff prefer face to face (physical learning)
* Practical learning for students

## Technology

* Digital divide
  * Not all have the same access to tech
* Good use of LMS leads to better learning
* Variety of technology (student and staff)





