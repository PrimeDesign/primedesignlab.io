---

title: Themes and Insights

subtitle: What we have yet to find out

date: 2017-09-01 00:00:00 +0000

tableofcontents: false

tags: [
"Alex",
"Hayley",
"Seona",
"Mikey",
"Thar",
"Sheldon"
]

---

* The class is only good as the tutor
  * Prepared classes creates validity of information.
  * Teachers need to be educated in preperation and delivery.
* Students like lessons that are well prepared
* Some students dont like group work
* Students prefer practical classes
* Recorded lectures cause students to skip class but also are useful for revision  -deep learning-
* Digital divide
  * Not everyone has the same access to technology.
* Technology is unreliable and throws of lesson when breaks.
  * Takes 10 min+ to fix.
* Students prefers less use of technology
* Learning is affected by comfort and activities
* Students like their own space to do activities
* Student want space that meet their work & social cultural, physical needs
* Students like quiet spaces to work
* Students dont tend to utilise notes + homework that is online
* Audio visual technology varies greatly
* Students dont like lectures because concentration of the student.
* People find multiple classes in the spare room difficult
* Students prefer 1 to 1/ small group learning with a tutor
* Moodle isn't user friendly
* If moodle is set out well, engagement is higher
* Variety of objects
* Creating a community (possible)


## Notes

* Technology would be good if it works
* Distance is a physical barrier

## Areas we need to define/ Look into to

* Different types of learners
* Good + prepared
  * What makes a good class?
  * What makes a prepared class
* Integrate technology
* Impact of comfort/ aesthetics
* Human needs from environment
* Objects are enablers
* What motivates a student
