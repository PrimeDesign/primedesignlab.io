---

title: What we dont know

subtitle: What we have yet to find out

date: 2017-08-16 00:00:00 +0000

tableofcontents: false

tags: [ "Alex", "Hayley", "Seona", "Mikey", "Thar", "Sheldon" ]

---

## What range of subjects taught in the new space

---

We need to know more

## Will there be prototyping spaces in the courses taught in the new building

---

We need to know more

## Will schools + departments will use the space

---

We need to know more

## Who will be staffing and managing the space

---

We need to know more

## We need more info from more disciplines

---

We need to know more

## How do we make presentations easier for students

---

We need to know more

## How does the space effect different cultures and levels

---

We need to know more

## why do people want open and flexible spaces

---

We need to know more

## How can we apply technology to support their needs

---

We need to know more

## How do smaller technology work

---

We need to know more

## What is optimal staff/tutor

---

We need to know more

## Does adaptive learning fit within wintec T+C Direction

---

We need to know more

## What activities are done in our communities

---

We need to know more

## What are breath of learning spaces

---

We need to know more

## What affects students to study in the hub

---

We need to know more

## What are the details of learning technologies

---

We need to know more

## Can we learn remotely

---

We need to know more

## What type of learning situations are there

---

We need to know more

## Human computer interface

---

We need to know more

## How much can we learn from VR

---

We need to know more

## What is the relation ship between VR and Real life

---

We need to know more

## What counts as as hands on experience

---

We need to know more

## What learning spaces work best

---

We need to know more