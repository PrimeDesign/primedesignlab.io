---

title: What we know

subtitle: What is stuck in our head

date: 2017-08-15 00:00:00 +0000

publishdate: 2017-08-15 00:00:00 +0000

tableofcontents: false

tags: [
"Alex",
"Hayley",
"Seona",
"Mikey",
"Thar",
"Sheldon"
]

---

Change from improving technology

---------------------

Emphasis on what we can do with it

---------------------

Maker space takes parts that are useful for a range of disciplines

---------------------

Mspaces are adaptable and add new technology

---------------------

Its a trend across education to have prototyping space

---------------------

M.S - technology - focus was on faster, bigger, now focus on what we can do with it

---------------------

Lots of our spaces on city campus are outdated

---------------------

Not enough quiet spaces computers

---------------------

Students are presenting idea/presentations a lot

---------------------

Students say computers are too close together (noise distraction)

---------------------

That its a trend to move towards open + flexible spaces

---------------------

People want quick/any support when they are learning eg tech

---------------------

Trend - people want a mixture of activity/services eg(cafe/libaries/computer rooms)

---------------------

Ken Robinson - Today is built on past technologies + systems

---------------------

Both teaching + learning can happen in one space

---------------------

Some areas are designated to speaslist disciplines

---------------------

Learning spaces can be inside or outside

---------------------

The tools are - multi functions + more functions eg iphone/computer

---------------------

Technology has become more integrated

---------------------

things are more accessible

---------------------

Trend use of vr/ar in education

---------------------

Trend collection of useful data / data mining to provide a well rounded view of the student performance - to assist the student + institution

---------------------

Trend pure research vs real world, to integrate real world into learning experience

---------------------

Trend smaller schools -> teacher can get to move student to help/coach

---------------------
Trend adaptive learning - customized by the technology journey

---------------------

The third place - places of learning

---------------------

Learning technology - supports learning teacher and assessment

---------------------

Learning spaces are a physical, virtual + conceptual spaces

---------------------

Trend More learning through VR, simulations, holographic's

---------------------

## The third place

---------------------

[The third Place]({{< ref "research/Key_Ideas_of_Space.md" >}})

## What types of learners are there

---------------------

![Types Of learners](/images/Types_Of_Learners.png)