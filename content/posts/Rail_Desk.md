---

title: "Rail Desk"

subtitle: "A Desk with a rail on it"

date: "2017-10-10"

publishdate: "2017-10-10"

tableofcontents: false

tags: [ "Prototype" ]

---

# Problems

## Portability

· Can it be moved- weight

## Space

· How many do we need

· May take up too much space

· Why circle

## QUIET

· How quiet can you make it

· Is quiet important

## Scalability

· May be to small

## Uses

· How can it be useful to other disciplines

## Feasibility

· Flat board on curved rail

# Benefits

· Object nature of design

· Quiet space

· Scalability – Good group work space

# Opportunities

· Space

o Can nest together

· Porfability

o Where?

· Scalability

o Bigge pods for group work

o Can it open out

· Materials

o Can it be made from glass

· Replacement

o For studio / Learning space

· Uses

o A range of attachments for specialised use

· Presentable

o Similar to class present

# Solutions

## Portability

· Wheels

· Lightweight

· Single outside power connection

## Space

Hex design

## Quiet

· Fabric walls

· Celling

Attachments

· Whiteboard

· Blackboard

· Clipboard

· ETC

## Feasibility

· Curved rail flat board

· Linear clock spring

## Breakfast

### I Like

*   Modular connections
*   The idea that is it scalable
*   Retractable wheels
*   Desk could have adjustable height
*   Queit space
*   Hexagon
*   Re-configurable, screens, whiteboard etc

### I wish

*   Chromecast/ Air play
*   Attachment for your own devices
*   I wish the space felt more open

### I wonder

*   Dedicated pods
*   I wonder if they can be collapsible
*   I wonder the size
*   I wonder the cost
*   Does it need to be a glass roof, can it be sound absorbing
*   Unzip walls / walls fold
*   Time limit
*   How do we address lighting
*   Too much / too enclosed
*   Desk space on the outside of the walls
*   Active noise cancelling
*   Life size prototype
*   How do teachers interact with the student in the rail desk